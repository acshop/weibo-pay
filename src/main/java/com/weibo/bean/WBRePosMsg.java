package com.weibo.bean;

/**
 * 消息回复-位置消息
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 下午1:16:56
 */
public class WBRePosMsg {

	private String longitude;
	private String latitude;

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "WBRePosMsg [longitude=" + longitude + ", latitude=" + latitude
				+ "]";
	}
}
