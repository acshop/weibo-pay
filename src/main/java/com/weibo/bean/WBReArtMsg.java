package com.weibo.bean;

import java.util.List;

/**
 * 图文消息回复
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 下午1:14:40
 */
public class WBReArtMsg {

	private List<Article> articles;

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "WBReArtMsg [articles=" + articles + "]";
	}
}
