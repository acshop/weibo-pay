package com.weibo.service;

import com.weibo.bean.WBMsg;
import com.weibo.bean.WBMsgData;
import com.weibo.bean.WBReMsg;

/**
 * 消息回复实现示例
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-5-8 下午3:11:21
 */
public class WeiBoReplyServiceImpl implements WeiBoReplyService {

	@Override
	public WBReMsg textTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

	@Override
	public WBReMsg positionTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

	@Override
	public WBReMsg voiceTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

	@Override
	public WBReMsg imageTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

	@Override
	public WBReMsg eventTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

	@Override
	public WBReMsg mentionTypeMsg(WBMsg<WBMsgData> msg) {
		// TODO Auto-generated method stub
		return new WBReMsg();
	}

}
